from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

import wave
from chunk import Chunk
from collections import namedtuple
import audioop
import builtins
import struct
import sys
import math

im = Image.open("samp.png")
image_width, image_height = im.size
print image_height
print image_width



Wave_write = wave.open("pilowav.wav", mode='wb')
Wave_write.setparams((2,2,44100,0,'NONE','not compressed'))
#The tuple should be (nchannels, sampwidth, framerate, nframes, comptype, compname), with values
data = []
FR = 44100
SEC = 10
length = FR*SEC
startFrq = 400
endFrq = 800
Height = 10
pixelHeight= 10
frq=420
MAX_DATA = 50;
for x in range(image_width): 
    data = []
    for y in range(image_height):
        r,g,b = im.getpixel((x,y))
        data.append((r + g + b ) / 3)
    data = np.fft.ifft(data)
    Samples = []                            
    for yy in range(image_height):
         Samples.append(MAX_DATA * data[yy])
    
    BinStr = "" #create a binary string
    for i in Samples:
        ii = math.sqrt(i.real*i.real+i.imag*i.imag)
        val = round(ii*20)
        if val >32767:
            val = 32767
        if val < -32768:
            val = -32768
        #print val
        BinStr += str(struct.pack('h', val))
    Wave_write.writeframesraw(BinStr)
    Wave_write.writeframesraw(BinStr)
    Wave_write.writeframesraw(BinStr)
Wave_write.close()


#data.append(math.sin(x*math.pi*frq/FR))


